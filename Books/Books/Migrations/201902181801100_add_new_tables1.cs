namespace Books.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_new_tables1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Authors", "Book_Id", c => c.Long());
            AddColumn("dbo.Genres", "Book_Id", c => c.Long());
            CreateIndex("dbo.Authors", "Book_Id");
            CreateIndex("dbo.Genres", "Book_Id");
            AddForeignKey("dbo.Authors", "Book_Id", "dbo.Books", "Id");
            AddForeignKey("dbo.Genres", "Book_Id", "dbo.Books", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Genres", "Book_Id", "dbo.Books");
            DropForeignKey("dbo.Authors", "Book_Id", "dbo.Books");
            DropIndex("dbo.Genres", new[] { "Book_Id" });
            DropIndex("dbo.Authors", new[] { "Book_Id" });
            DropColumn("dbo.Genres", "Book_Id");
            DropColumn("dbo.Authors", "Book_Id");
        }
    }
}
