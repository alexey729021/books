namespace Books.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_new_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                        Text = c.String(),
                        WritingYear = c.Long(nullable: false),
                        PublicationYear = c.Long(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Language_Id = c.Long(),
                        OriginalLanguage_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Languages", t => t.Language_Id)
                .ForeignKey("dbo.Languages", t => t.OriginalLanguage_Id)
                .Index(t => t.Language_Id)
                .Index(t => t.OriginalLanguage_Id);
            
            CreateTable(
                "dbo.Genres",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "OriginalLanguage_Id", "dbo.Languages");
            DropForeignKey("dbo.Books", "Language_Id", "dbo.Languages");
            DropIndex("dbo.Books", new[] { "OriginalLanguage_Id" });
            DropIndex("dbo.Books", new[] { "Language_Id" });
            DropTable("dbo.Genres");
            DropTable("dbo.Books");
            DropTable("dbo.Authors");
        }
    }
}
