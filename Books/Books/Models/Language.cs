﻿namespace Books.Models
{
    public class Language
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}