﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Books.Models
{
    public class Book
    {
        public long Id { get; set; }       

        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public string Text { get; set; }

        public long WritingYear { get; set; }

        public long PublicationYear { get; set; }

        public ICollection<Genre> Genres { get; set; }

        public Language Language { get; set; }

        public Language OriginalLanguage { get; set; }

        public ICollection<Author> Authors { get; set; }

        public bool Status { get; set; }

        //public RightHolder RightHolder { get; set; }
    }
}